# Discrete Event Simulation

Code is found in the 'Discrete Event Simulation' folder.  
The 'Map' folder is used to store the zone plans in the form of spreadsheets of nodes and edges.


# Fitness and Optimization Functions

We optimize existing patrol maps by the method of gradient descent.
Code for the optimization function is found in the `evaluation and optimization` folder.
It is written in Python and makes heavy use of the modules `Fiona` and `Shapely`, which import and manipulate GIS data, respectively.
Data for these functions cannot be released to the public and thus is not included in this database.