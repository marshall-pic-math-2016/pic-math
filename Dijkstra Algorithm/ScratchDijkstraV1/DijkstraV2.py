from collections import defaultdict, deque
from matplotlib import pyplot as plt
import math
import networkx as nx

class Graph:
    def __init__(self):
        self.nodes = set()
        self.edges = defaultdict(list)
        self.distances = {}

    def add_node(self, value):                              #Adds a value to the set of nodes
        self.nodes.add(value)

    def add_edge(self, from_node, to_node, distance):       #Adds an edge between from and to_nodes
        self.edges[from_node].append(to_node)
        self.edges[to_node].append(from_node)
        self.distances[(from_node, to_node)] = distance     #Sets length of edge to specified distance
        self.distances[(to_node, from_node)] = distance

    def dijkstra(graph, initial):
        visited = {initial:0}                                   #create dictionary of visited nodes
        path = {}

        nodes = set(graph.nodes)

        while nodes:                                            #While there are nodes left in node set,
            min_node = None                                     #
            for node in nodes:                                  #Evaluate each node in set
                if node in visited:                             #
                    if min_node is None:                        #
                        min_node = node                         #Set current node
                    elif visited[node] < visited[min_node]:     #
                        min_node = node                         #
            if min_node is None:
                break

            nodes.remove(min_node)                              #remove current node from list of nodes
            current_weight = visited[min_node]

            for edge in graph.edges[min_node]:                                  #for all edges connected
                                                                                # to the current node:
                weight = current_weight + graph.distances[(min_node,edge)]      #add current weight to distance
                                                                                #between nodes, if already visited
                if edge not in visited or weight < visited[edge]:               #set weight to edge length if not
                                                                                # visited yet
                    visited[edge] = weight
                    path[edge] = min_node
        return visited, path



def shortest_path(graph, origin, destination):
    visited, paths = Graph.dijkstra(graph, origin)
    full_path = deque()
    #print(destination)
    _destination = paths[str(destination)]

    while _destination != origin:
        full_path.appendleft(_destination)
        _destination = paths[_destination]

    full_path.appendleft(origin)
    full_path.append(destination)

    return visited[destination], list(full_path)




if __name__ == '__main__':
    graph = Graph()
    plt.clf()
    x = []
    y = []

    nodes = []                                #list of nodes and --> connected nodes
    nodes.append((0,0,0))                     #Node 0 --> Node 1, 2
    nodes.append((2,0,1))                     #1 --> 4
    nodes.append((0,1,2))                     #2 --> 3,5
    nodes.append((1,1,3))                     #3 --> 4,6
    nodes.append((2,1,4))                     #4 --> 7
    nodes.append((0,2,5))                     #5 --> 6,8
    nodes.append((1,2,6))                     #6 --> 7,9
    nodes.append((2,2,7))                     #7 --> 9
    nodes.append((0,3,8))                     #8 --> 9
    nodes.append((1,3,9))


    for node in range(0,len(nodes)):
        graph.add_node(str(nodes[node][2]))
        x.append(nodes[node][0])
        y.append(nodes[node][1])

    #for node in ['0','1','2','3','4','5']:
        #graph.add_node(node)
    def dist(node1, node2):
        dist = math.sqrt( (nodes[node2][0]-nodes[node1][0])**2 + (nodes[node2][1]-nodes[node1][1])**2 )
        return dist

    dist01 = dist(0,1)
    dist02 = dist(0,2)
    dist14 = dist(1,4)
    dist23 = dist(2,3)
    dist25 = dist(2,5)
    dist34 = dist(3,4)
    dist36 = dist(3,6)
    dist47 = dist(4,7)
    dist56 = dist(5,6)
    dist58 = dist(5,8)
    dist67 = dist(6,7)
    dist69 = dist(6,9)
    dist79 = dist(7,9)
    dist89 = dist(8,9)





    graph.add_edge(str(nodes[0][2]),str(nodes[1][2]),dist01)
    graph.add_edge(str(nodes[0][2]),str(nodes[2][2]),dist02)
    graph.add_edge(str(nodes[1][2]),str(nodes[4][2]),dist14)
    graph.add_edge(str(nodes[2][2]),str(nodes[3][2]),dist23)
    graph.add_edge(str(nodes[2][2]),str(nodes[5][2]),dist25)
    graph.add_edge(str(nodes[3][2]),str(nodes[4][2]),dist34)
    graph.add_edge(str(nodes[3][2]),str(nodes[6][2]),dist36)
    graph.add_edge(str(nodes[4][2]),str(nodes[7][2]),dist47)
    graph.add_edge(str(nodes[5][2]),str(nodes[6][2]),dist56)
    graph.add_edge(str(nodes[5][2]),str(nodes[8][2]),dist58)
    graph.add_edge(str(nodes[6][2]),str(nodes[7][2]),dist67)
    graph.add_edge(str(nodes[6][2]),str(nodes[9][2]),dist69)
    graph.add_edge(str(nodes[7][2]),str(nodes[9][2]),dist79)
    graph.add_edge(str(nodes[8][2]),str(nodes[9][2]),dist89)

    result = shortest_path(graph, '8', '3')


    print(result)


    #Attempt at plotting nodes/edges.  Edges can be plotted as lines, should be simple
    #would prefer to use networkx, but their documentation gives me a headache
    #

    """
    #Attempt to add in a graph of points / shortest route
    xmax = 0
    ymax = 0
    for value in range(0,len(nodes)):
        plt.plot(nodes[value][0],nodes[value][1],'or')
        plt.text((nodes[value][0]-0.25),(nodes[value][1]-0.25),nodes[value][2])
        if(x[value]>xmax):
            xmax = x[value]
        if(y[value]>ymax):
            ymax = y[value]
    plt.axis([0,xmax+3,0,ymax+3])
    #plt.plot(1,1)
    plt.show()
    """