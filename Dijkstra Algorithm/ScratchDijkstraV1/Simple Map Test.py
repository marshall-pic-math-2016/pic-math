import math

import DijkstraV2

graph = DijkstraV2.Graph()

x = []
y = []
nodes = []
                                #list of nodes and --> connected nodes
nodes.append((0,0,0))                     #Node 0 --> Node 1, 2
nodes.append((2,0,1))                     #1 --> 4
nodes.append((0,1,2))                     #2 --> 3,5
nodes.append((1,1,3))                     #3 --> 4,6
nodes.append((2,1,4))                     #4 --> 7
nodes.append((0,2,5))                     #5 --> 6,8
nodes.append((1,2,6))                     #6 --> 7,9
nodes.append((2,2,7))                     #7 --> 9
nodes.append((0,3,8))                     #8 --> 9
nodes.append((1,3,9))


for node in range(0,len(nodes)):
        graph.add_node(str(nodes[node][2]))
        x.append(nodes[node][0])
        y.append(nodes[node][1])

def dist(node1, node2):
        dist = math.sqrt( (nodes[node2][0]-nodes[node1][0])**2 + (nodes[node2][1]-nodes[node1][1])**2 )
        return dist

dist01 = dist(0,1)
dist02 = dist(0,2)
dist14 = dist(1,4)
dist23 = dist(2,3)
dist25 = dist(2,5)
dist34 = dist(3,4)
dist36 = dist(3,6)
dist47 = dist(4,7)
dist56 = dist(5,6)
dist58 = dist(5,8)
dist67 = dist(6,7)
dist69 = dist(6,9)
dist79 = dist(7,9)
dist89 = dist(8,9)

graph.add_edge(str(nodes[0][2]),str(nodes[1][2]),dist01)
graph.add_edge(str(nodes[0][2]),str(nodes[2][2]),dist02)
graph.add_edge(str(nodes[1][2]),str(nodes[4][2]),dist14)
graph.add_edge(str(nodes[2][2]),str(nodes[3][2]),dist23)
graph.add_edge(str(nodes[2][2]),str(nodes[5][2]),dist25)
graph.add_edge(str(nodes[3][2]),str(nodes[4][2]),dist34)
graph.add_edge(str(nodes[3][2]),str(nodes[6][2]),dist36)
graph.add_edge(str(nodes[4][2]),str(nodes[7][2]),dist47)
graph.add_edge(str(nodes[5][2]),str(nodes[6][2]),dist56)
graph.add_edge(str(nodes[5][2]),str(nodes[8][2]),dist58)
graph.add_edge(str(nodes[6][2]),str(nodes[7][2]),dist67)
graph.add_edge(str(nodes[6][2]),str(nodes[9][2]),dist69)
graph.add_edge(str(nodes[7][2]),str(nodes[9][2]),dist79)
graph.add_edge(str(nodes[8][2]),str(nodes[9][2]),dist89)


result = DijkstraV2.shortest_path(graph, '8', '3')

print(result)

