#I haven't completely finished commenting this code yet.  I don't know how it all works yet.
#But it does work very nicely.  I should probably credit whoever I stole this from.


from collections import defaultdict, deque

class Graph:                                            #Define a class called Graph
    def __init__(self):                                 #Locations to store nodes, edges and distances
        self.nodes = set()                              #Set
        self.edges = defaultdict(list)                  #Default dictionary
        self.distances = {}                             #Dictionary

    def add_node(self, value):                              #Adds a value to the set of nodes
        self.nodes.add(value)

    def add_edge(self, from_node, to_node, distance):       #Adds an edge between from and to_nodes
        self.edges[from_node].append(to_node)               #Edge goes both directions
        self.edges[to_node].append(from_node)
        self.distances[(from_node, to_node)] = distance     #Sets length of edge to specified distance
        self.distances[(to_node, from_node)] = distance     #Edge goes both ways

    def dijkstra(graph, initial):
        visited = {initial:0}                                   #create dictionary of visited nodes
        path = {}                                               #Empty dictionary for path

        nodes = set(graph.nodes)                                #Create set of all created nodes

        while nodes:                                            #While there are nodes left in node set,
            min_node = None                                     #No node has the closest distance
            for node in nodes:                                  #Evaluate each node in set
                if node in visited:                             #If the node has been visited already
                    if min_node is None:                        #Set current node as closest if none have been evaluated
                        min_node = node                         #Set current node as closest
                    elif visited[node] < visited[min_node]:     #
                        min_node = node                         #
            if min_node is None:
                break                                           #Ends "while nodes:" loop above

            nodes.remove(min_node)                              #remove current node from list of nodes
            current_weight = visited[min_node]

            for edge in graph.edges[min_node]:                                  #for all edges connected
                                                                                # to the current node:
                weight = current_weight + graph.distances[(min_node,edge)]      #add current weight to distance
                                                                                #between nodes, if already visited
                if edge not in visited or weight < visited[edge]:               #set weight to edge length if not
                                                                                # visited yet
                    visited[edge] = weight
                    path[edge] = min_node
        return visited, path



def shortest_path(graph, origin, destination):
    visited, paths = Graph.dijkstra(graph, origin)
    full_path = deque()
    _destination = paths[str(destination)]

    while _destination != origin:
        full_path.appendleft(_destination)
        _destination = paths[_destination]

    full_path.appendleft(origin)
    full_path.append(destination)

    return visited[destination], list(full_path)