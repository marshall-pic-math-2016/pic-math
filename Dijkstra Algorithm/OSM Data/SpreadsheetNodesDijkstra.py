#####Enter data here####

nodesfile = 'SmallMap/Nodes.csv'                  #CSV file containing nodes
edgesfile = 'SmallMap/Edges.csv'                  #CSV file containing edges

startnode = '154782612'                                    #Number of start node
destnode = '154777814'                                     #Number of destination node


#####





import Dijkstra                         #Imports classes and functions from the Dijkstra.py file in this directory
import numpy as np                      #Numpy is used to parse CSV data into numpy arrays

graph = Dijkstra.Graph()                #Creates an object from the Dijkstra Graph class


#These lines generate numpy arrays from CSV files.

#Arguments:
# directory to csv file
# delimiter that divides columns from each other (commas for CSV data)
# the column to parse that particular array of data from
# the number of lines in the header that should be skipped (line of text headings at top)

nodes = np.genfromtxt(nodesfile,delimiter=',',usecols=0,skip_header=1)              #Array of all node names
lat = np.genfromtxt(nodesfile,delimiter=',',usecols=1, skip_header=1)               #Array of latitudes
long = np.genfromtxt(nodesfile,delimiter=',',usecols=2,skip_header=1)               #and longitudes

edge_id = np.genfromtxt(edgesfile,delimiter=',',usecols=0,skip_header=1)            #Array of edge names
edge_sn = np.genfromtxt(edgesfile,delimiter=',',usecols=1,skip_header=1)            #Start nodes for each edge
edge_dn = np.genfromtxt(edgesfile,delimiter=',',usecols=2,skip_header=1)            #and destination nodes
edge_length = np.genfromtxt(edgesfile,delimiter=',',usecols=3,skip_header=1)        #Length of edge in meters


for node in range(0,len(nodes)):                            #For the number of nodes in the nodes array,

    graph.add_node(str(int(nodes[node])))                   #Add the node name to the graph

for n in range(0,len(edge_id)):                                                 #For the number of edges,
    graph.add_edge(str(int(edge_sn[n])),str(int(edge_dn[n])),edge_length[n])    #Add the edge start node, destination node,
                                                                                #and weighting that should be used
                                                                                #(distance for now, later will be time)

result = Dijkstra.shortest_path(graph, startnode, destnode)                     #Runs dijkstra function for the specified
                                                                                #Start and destination nodes

print(result)