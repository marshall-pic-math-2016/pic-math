# Dijkstra Algorithm Codes

/ScratchDijkstraV1 has some of my older attempts at using the algorithm.  Probably unimportant, but doesn't hurt to save them.

The file Original Algorithm Code has an implementation of the algorithm that uses a shapefile.  I haven't gotten it to work yet and I don't think we'll use it anyway.

The /Dijkstra for Lat:Long OSM Data is the relevant one.

/SmallMap has a small OSM file of some streets in Huntington.  You can import that into QGIS to view it using Vector-->OpenStreetMap.  Look up a tutorial for this, it takes a few steps but is pretty simple.  
The other files in this folder are the spreadsheet files created by osm4routing.  Right now, this seems like the best way to get our street data into simulations.  Feel free to check out osm4routing for yourself, but I'll warn you that it has some dependencies that proved difficult to install.  Took me all damn day.

The Dijkstra.py file is the actual script that runs the Algorithm.

The SpreadsheetNodesDijkstra.py is the file I've written that parses in spreadsheet data and runs the Algorithm on whatever is there.  The file paths currently there should work, but if you want to use a different spreadsheet for nodes, change the paths.  You can change the nodes to any nodes that are in the small map.

Next step:  Latitude/longitude integration.

*Input a lat/long pairs for start and end, and find closest nodes to both, then find route.  Should be relatively simple, just a distance function.



