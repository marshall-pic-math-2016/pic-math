import simpy
import numpy as np
from random import randint
import Dijkstra
import re
                                            #Speeds in miles per hour
highspeeds = [25,30,35,40,50,65]            #Higher speed for Dijkstra event routing
lowspeeds = [10,15,20,25,35,60]             #Lower cruising speed

graph = Dijkstra.Graph()
nodesfile = 'EditedMap/nodesedited.csv'                  #CSV file containing nodes
edgesfile = 'EditedMap/edgesedited.csv'                  #CSV file containing edges

outfile = open('testroute.csv','w')
outfile.write('node,latitude,longitude,time')
outfile.write('\n')

for n in range(0,len(highspeeds)):              #Change speeds to meters/hr
    highspeeds[n] = highspeeds[n]*1609.34
    lowspeeds[n] = lowspeeds[n]*1609.34

nodes = np.genfromtxt(nodesfile,delimiter=',',usecols=0,skip_header=1)              #Array of all node names
lat = np.genfromtxt(nodesfile,delimiter=',',usecols=1, skip_header=1)               #Array of latitudes
long = np.genfromtxt(nodesfile,delimiter=',',usecols=2,skip_header=1)               #and longitudes

edge_id = np.genfromtxt(edgesfile,delimiter=',',usecols=0,skip_header=1)            #Array of edge names
edge_sn = np.genfromtxt(edgesfile,delimiter=',',usecols=1,skip_header=1)            #Start nodes for each edge
edge_dn = np.genfromtxt(edgesfile,delimiter=',',usecols=2,skip_header=1)            #and destination nodes
edge_length = np.genfromtxt(edgesfile,delimiter=',',usecols=3,skip_header=1)        #Length of edge in meters
edge_road = np.genfromtxt(edgesfile,delimiter=',',usecols=4,skip_header=1)          #
edge_oneway = np.genfromtxt(edgesfile,delimiter=',',usecols=5,skip_header=1)        #One way value


for node in range(0,len(nodes)):                                                   #Add nodes to graph
    graph.add_node(str(int(nodes[node])))


for n in range(0,len(edge_id)):                                         #Add edges to graph
    high_time = edge_length[n] / highspeeds[int(edge_road[n]-1)],       #Create time-weighting based on road length
    low_time = edge_length[n] / lowspeeds[int(edge_road[n]-1)]          #And speed of driving

    if edge_oneway[n] != 0 and edge_road[n] != 0:                                             #Add one-way and two-way roads
        graph.add_edge(str(int(edge_sn[n])),str(int(edge_dn[n])),low_time, high_time)         #to graph
    else:
        graph.add_edge_oneway(str(int(edge_sn[n])),str(int(edge_dn[n])),low_time, high_time)


class Car(object):                   #Car process

    def __init__(self, env):
        self.env = env
        self.action = env.process(self.patrol())
        self.currentnode = nodes[randint(0,len(nodes))]

    def patrol(self):
        while True:
            if env.now == 0:                            #Assign random start node at beginning
                Car.currentnode = nodes[randint(0,len(nodes))]

            for line in open(nodesfile):
                if re.search(str(int(Car.currentnode)),line):
                    line = line.strip()
                    outfile.write('%s, %s \n' % (line, str(env.now)))       #Write node info to outfile
            print('Leaving node %d at time %d' % (Car.currentnode, env.now))
            dest_node, time = Dijkstra.random_connected_node(graph,str(int(Car.currentnode)))    #Returns a random connected node
                                                                                    #and the travel time to it in minutes
            print('Travelling to node %s with travel time %s' %(dest_node, str(time)))
            yield env.timeout(time)                         #Timeout for duration of travel time
            Car.currentnode = int(dest_node)                #Set new node as start node for new travel path

env = simpy.Environment()
car = Car(env)
env.run(until=60000)

outfile.close()