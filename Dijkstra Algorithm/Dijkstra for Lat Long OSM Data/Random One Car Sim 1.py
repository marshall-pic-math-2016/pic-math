import simpy
import numpy as np
from random import randint
import Dijkstra
import re


graph = Dijkstra.Graph()
nodesfile = 'SmallMap/Nodes.csv'                  #CSV file containing nodes
edgesfile = 'SmallMap/Edges.csv'                  #CSV file containing edges

outfile = open('/Users/Matt/Desktop/testroute.csv','w')
outfile.write('node,longitude,latitude')
outfile.write('\n')


nodes = np.genfromtxt(nodesfile,delimiter=',',usecols=0,skip_header=1)              #Array of all node names
long = np.genfromtxt(nodesfile,delimiter=',',usecols=1, skip_header=1)               #Array of latitudes
lat = np.genfromtxt(nodesfile,delimiter=',',usecols=2,skip_header=1)               #and longitudes

edge_id = np.genfromtxt(edgesfile,delimiter=',',usecols=0,skip_header=1)            #Array of edge names
edge_sn = np.genfromtxt(edgesfile,delimiter=',',usecols=1,skip_header=1)            #Start nodes for each edge
edge_dn = np.genfromtxt(edgesfile,delimiter=',',usecols=2,skip_header=1)            #and destination nodes
edge_length = np.genfromtxt(edgesfile,delimiter=',',usecols=3,skip_header=1)        #Length of edge in meters



for node in range(0,len(nodes)):                                                   #Add nodes to graph

    graph.add_node(str(int(nodes[node])))

for n in range(0,len(edge_id)):                                                 #Add edges to graph

    graph.add_edge(str(int(edge_sn[n])),str(int(edge_dn[n])),edge_length[n])



def car(env):                   #Car process
    while True:
        if env.now == 0:                            #Assign random start node at beginning
            start_node = randint(0,len(nodes))
        print('Leaving node %d at time %d' % (nodes[start_node], env.now))
        dest_node = randint(0,len(nodes))
        if start_node == dest_node:
            break
        travel_route = Dijkstra.shortest_path(graph, str(int(nodes[start_node])), str(int(nodes[dest_node])))
        travel_dist = travel_route[0]
        print('Travelling to node %d with travel distance %d' %(nodes[dest_node], travel_dist))
        for k in range(0,len(travel_route[1])):
            for line in open(nodesfile):
                if re.search(str(travel_route[1][k]),line):
                    outfile.write(line)
                    outfile.write('\n')

        yield env.timeout(travel_dist)
        start_node = dest_node


env = simpy.Environment()
env.process(car(env))
env.run(until=10000)

outfile.close()
