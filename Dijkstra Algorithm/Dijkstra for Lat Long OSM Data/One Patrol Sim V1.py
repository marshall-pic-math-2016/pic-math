import simpy
import numpy as np
from random import randint
import Dijkstra
import re
                                            #Speeds in miles per hour
highspeeds = [25,30,35,40,50,80]            #Higher speed for Dijkstra event routing
lowspeeds = [10,15,20,25,35,65]             #Lower cruising speed

graph = Dijkstra.Graph()
nodesfile = 'EditedMap/nodesedited.csv'                  #CSV file containing nodes
edgesfile = 'EditedMap/edgesedited.csv'                  #CSV file containing edges

for n in range(0,len(highspeeds)):              #Change speeds to meters/hr
    highspeeds[n] = highspeeds[n]*1609.34
    lowspeeds[n] = lowspeeds[n]*1609.34

nodes = np.genfromtxt(nodesfile,delimiter=',',usecols=0,skip_header=1)              #Array of all node names
lat = np.genfromtxt(nodesfile,delimiter=',',usecols=1, skip_header=1)               #Array of latitudes
long = np.genfromtxt(nodesfile,delimiter=',',usecols=2,skip_header=1)               #and longitudes

edge_id = np.genfromtxt(edgesfile,delimiter=',',usecols=0,skip_header=1)            #Array of edge names
edge_sn = np.genfromtxt(edgesfile,delimiter=',',usecols=1,skip_header=1)            #Start nodes for each edge
edge_dn = np.genfromtxt(edgesfile,delimiter=',',usecols=2,skip_header=1)            #and destination nodes
edge_length = np.genfromtxt(edgesfile,delimiter=',',usecols=3,skip_header=1)        #Length of edge in meters
edge_road = np.genfromtxt(edgesfile,delimiter=',',usecols=4,skip_header=1)          #
edge_oneway = np.genfromtxt(edgesfile,delimiter=',',usecols=5,skip_header=1)        #One way value


for node in range(0,len(nodes)):                                                   #Add nodes to graph
    graph.add_node(str(int(nodes[node])))


for n in range(0,len(edge_id)):                                         #Add edges to graph
    lt = edge_length[n] / lowspeeds[int(edge_road[n]-1)]          #And speed of driving
    ht = edge_length[n] / highspeeds[int(edge_road[n]-1)]
    if edge_oneway[n] != 0:                                          #Add one-way and two-way roads
        graph.add_edge(str(int(edge_sn[n])),str(int(edge_dn[n])),lt, ht)         #to graph
    else:
        graph.add_edge_oneway(str(int(edge_sn[n])),str(int(edge_dn[n])),lt, ht)

startnode = randint(0,len(nodes))
endnode = randint(0,len(nodes))

#print Dijkstra.shortest_path(graph, str(int(nodes[startnode])), str(int(nodes[endnode])))*60,
#print int(nodes[startnode]), int(nodes[endnode])
print Dijkstra.shortest_path(graph, str(int(154805803)), str(int(154785652)))* 60