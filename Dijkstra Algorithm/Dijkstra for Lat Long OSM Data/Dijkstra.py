#I haven't completely finished commenting this code yet.  I don't know how it all works yet.
#But it does work very nicely.  I should probably credit whoever I stole this from.


from collections import defaultdict, deque
import numpy as np
from random import randint

class Graph:                                            #Define a class called Graph
    def __init__(self):                                 #Locations to store nodes, edges and distances
        self.nodes = set()                              #Set
        self.edges = defaultdict(list)                  #Default dictionary
        self.distances = {}                             #Dictionary
        self.lowtime = {}
        self.hightime={}

    def add_node(self, value):                              #Adds a value to the set of nodes
        self.nodes.add(value)

    def add_edge(self, from_node, to_node, low_time, high_time):       #Adds an edge between from and to_nodes
        self.edges[from_node].append(to_node)               #Edge goes both directions
        self.edges[to_node].append(from_node)
        self.lowtime[(to_node, from_node)] = low_time
        self.lowtime[(from_node,to_node)] = low_time
        self.hightime[(to_node, from_node)] = high_time
        self.hightime[(from_node, to_node)] = high_time

    def add_edge_oneway(self,from_node, to_node, low_time, high_time):
        self.edges[from_node].append(to_node)
        self.lowtime[(from_node, to_node)] = low_time
        self.hightime[(from_node, to_node)] = high_time


    def dijkstra(graph, initial):
        visited = {initial:0}                                   #create dictionary of visited nodes
        path = {}                                               #Empty dictionary for path

        nodes = set(graph.nodes)                                #Create set of all created nodes

        while nodes:                                            #While there are nodes left in node set,
            min_node = None                                     #No node has the closest distance
            for node in nodes:                                  #Evaluate each node in set
                if node in visited:                             #If the node has been visited already
                    if min_node is None:                        #Set current node as closest if none have been evaluated
                        min_node = node                         #Set current node as closest
                    elif visited[node] < visited[min_node]:     #
                        min_node = node                         #
            if min_node is None:
                break                                           #Ends "while nodes:" loop above

            nodes.remove(min_node)                              #remove current node from list of nodes
            current_weight = visited[min_node]



            for edge in graph.edges[min_node]:                                  #for all edges connected
                weight = current_weight + graph.hightime[(min_node,edge)]      #add current weight to distance
                #weight = current_weight + graph.lowtime[(min_node,edge)]
                                                                              #between nodes, if already visited
                if edge not in visited or weight < visited[edge]:               #set weight to edge length if not
                                                                                # visited yet
                    visited[edge] = weight
                    path[edge] = min_node

        return visited, path

def geocalc(lat0, lon0, lat1, lon1):
    """Return the distance (in km) between two points in
    geographical coordinates."""

    EARTH_R = 6372.8
    lat0 = np.radians(lat0)
    lon0 = np.radians(lon0)
    lat1 = np.radians(lat1)
    lon1 = np.radians(lon1)
    dlon = lon0 - lon1
    y = np.sqrt(
        (np.cos(lat1) * np.sin(dlon)) ** 2
         + (np.cos(lat0) * np.sin(lat1)
         - np.sin(lat0) * np.cos(lat1) * np.cos(dlon)) ** 2)
    x = np.sin(lat0) * np.sin(lat1) + \
        np.cos(lat0) * np.cos(lat1) * np.cos(dlon)
    c = np.arctan2(y, x)
    return EARTH_R * c

def shortest_path(graph, origin, destination):
    visited, paths = Graph.dijkstra(graph, origin)
    full_path = deque()
    _destination = paths[str(destination)]

    while _destination != origin:
        full_path.appendleft(_destination)
        _destination = paths[_destination]

    full_path.appendleft(origin)
    full_path.append(destination)

    return visited[destination] ###list(full_path) - can return this too

def random_connected_node(graph, startnode):
    if len(graph.edges[startnode]) == 0:
        print startnode
    dest_node_number = randint(0,len(graph.edges[startnode])-1)     #chooses a random connected node
    dest_node = graph.edges[startnode][dest_node_number]
    time = graph.lowtime[(startnode, dest_node)] * 60               #Calculates travel time to new node
    return dest_node, time                                  #Returns node id and travel time