import simpy
import numpy as np
from random import randint
import Dijkstra
import re


graph = Dijkstra.Graph()
nodesfile = 'EditedMap/nodesedited.csv'                  #CSV file containing nodes
edgesfile = 'EditedMap/edges.csv'                  #CSV file containing edges

outfile = open('testroute.csv','w')
outfile.write('node,longitude,latitude,time')
outfile.write('\n')


nodes = np.genfromtxt(nodesfile,delimiter=',',usecols=0,skip_header=1)              #Array of all node names
long = np.genfromtxt(nodesfile,delimiter=',',usecols=1, skip_header=1)               #Array of latitudes
lat = np.genfromtxt(nodesfile,delimiter=',',usecols=2,skip_header=1)               #and longitudes

edge_id = np.genfromtxt(edgesfile,delimiter=',',usecols=0,skip_header=1)            #Array of edge names
edge_sn = np.genfromtxt(edgesfile,delimiter=',',usecols=1,skip_header=1)            #Start nodes for each edge
edge_dn = np.genfromtxt(edgesfile,delimiter=',',usecols=2,skip_header=1)            #and destination nodes
edge_length = np.genfromtxt(edgesfile,delimiter=',',usecols=3,skip_header=1)        #Length of edge in meters
edge_road = np.genfromtxt(edgesfile,delimiter=',',usecols=4,skip_header=1)          #
edge_oneway = np.genfromtxt(edgesfile,delimiter=',',usecols=5,skip_header=1)        #One way value





for node in range(0,len(nodes)):                                                   #Add nodes to graph

    graph.add_node(str(int(nodes[node])))

for n in range(0,len(edge_id)):                                                 #Add edges to graph
    if edge_oneway[n] != 0 and edge_road[n] != 0:
        graph.add_edge(str(int(edge_sn[n])),str(int(edge_dn[n])),edge_length[n])
    else:
        graph.add_edge_oneway(str(int(edge_sn[n])),str(int(edge_dn[n])),edge_length[n])



def car(env):                   #Car process
    while True:
        if env.now == 0:                            #Assign random start node at beginning
            start_node = nodes[randint(0,len(nodes))]
        for line in open(nodesfile):
            if re.search(str(int(start_node)),line):
                line = line.strip()
                outfile.write('%s, %d \n' % (line, env.now))
        print('Leaving node %d at time %d' % (start_node, env.now))
        dest_node, dist = Dijkstra.random_connected_node(graph,str(int(start_node)))

        print('Travelling to node %s with travel distance %d' %(dest_node, dist))
        yield env.timeout(dist)
        start_node = int(dest_node)


env = simpy.Environment()
env.process(car(env))
env.run(until=1000)


outfile.close()