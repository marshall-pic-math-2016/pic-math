import simpy

def driver(env,car):                #New driver process with reference to car's process
    yield env.timeout(3)            #Waits 3 seconds,
    car.action.interrupt()          #then interrupts car.action process

class Car(object):
    def __init__(self, env):
        self.env = env
        self.action = env.process(self.run())

    def run(self):
        while True:
            print('Start parking and charging at %d' %(self.env.now))   #Prints current time
            charge_duration = 5                         #Sets time duration of charge process
            try:                                        #Attempts to run charge process
                yield self.env.process(self.charge(charge_duration))
            except simpy.Interrupt:                     #When we receive an interrupt, stop charging
                print('Was interrupted.')               #and return to driving

            print('Start driving at %d' %(self.env.now))    #now return to driving for time 2
            trip_duration = 2
            yield self.env.timeout(trip_duration)

    def charge(self, duration):                         #define charge process for duration
        yield self.env.timeout(duration)                #Timeout until that time reached

env = simpy.Environment()
car = Car(env)
car2 = Car(env)
env.process(driver(env,car))
#env.process(driver(env,car2))
env.run(until=15)