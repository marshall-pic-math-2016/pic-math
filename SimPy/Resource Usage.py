import simpy

def car(env, name, bcs, driving_time, charge_duration):
                                                    #Car process now needs multiple arguments
    yield env.timeout(driving_time)                 #Timeout until specified driving_time argument

    print('%s arriving at %d' % (name, env.now))
    with bcs.request() as req:                 #Car owns the resource it requested until it releases it
                                               #"with" command makes sure it is released automatically
        yield req

        print('%s starting to charge at %d' % (name, env.now))
        yield env.timeout(charge_duration)
        print('%s leaving the bcs at %s' % (name, env.now)) #Resources is released, next waiting process
                                                            #gets the resource (FIF0)



env = simpy.Environment()
bcs = simpy.Resource(env, capacity=2)           #Create a resource with capacity 2
for i in range(4):
    env.process(car(env, 'Car %d' % i, bcs, i*2, 5))  #Create 4 cars, pass arguments to each
env.run()



#Car 0 arriving at 0
#Car 0 starting to charge at 0
#Car 1 arriving at 2
#Car 1 starting to charge at 2
#Car 2 arriving at 4
#Car 0 leaving the bcs at 5
#Car 2 starting to charge at 5
#Car 3 arriving at 6
#Car 1 leaving the bcs at 7
#Car 3 starting to charge at 7
#Car 2 leaving the bcs at 10
#Car 3 leaving the bcs at 12