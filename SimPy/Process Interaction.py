import simpy

class Car(object):
    def __init__(self,env):                                 #Define a "Car" class
        self.env = env
        self.action = env.process(self.run())

    def run(self):
        while True:
            print('Start parking and charging at %d' %(self.env.now))      #Prints start of park + charge time
            charge_duration = 5
            yield self.env.process(self.charge(charge_duration))            #Runs outside charge process
                                                                            #for charge_Duration = 5
            print('Start driving at %d' %(self.env.now))                    #Prints start of driving
            trip_duration = 2
            yield self.env.timeout(trip_duration)                           #Timeout process of 2

    def charge(self,duration):                                              #Charge function, takes
        yield self.env.timeout(duration)                                    #duration argument

env = simpy.Environment()                                           #Starts environment
car = Car(env)                                                      #Starts car class
env.run(until = 15)

#Output
#Start parking and charging at 0
#Start driving at 5
#Start parking and charging at 7
#Start driving at 12
#Start parking and charging at 14