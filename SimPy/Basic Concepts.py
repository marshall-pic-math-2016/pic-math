import simpy

def car(env):                                   #Define "car" as a process of Environment "env"
                                                #Car process requires reference to Environment (env)
    while True:
        print('Start parking at %d' % env.now)  #Prints start parking time
        parking_duration = 5                    #Parking time is = 5
        yield env.timeout(parking_duration)     #Timeout of 5 is yielded

        print('Start driving at %d' % env.now)  #Prints start driving time
        trip_duration = 2                       #Driving for time = 2
        yield env.timeout(trip_duration)        #Yields trip_duration timeout

env = simpy.Environment()                       #Create instance of "Environment"
env.process(car(env))                           #Start car process and add it to env
env.run(until=15)                               #Run car process until time 15

#Results:
#Start parking at 0
#Start driving at 5
#Start parking at 7
#Start driving at 12
#Start parking at 14
