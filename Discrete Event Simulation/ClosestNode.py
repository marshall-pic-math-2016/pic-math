""" For a list of geocoded latitudes and longitudes,
finds the closest available node in the specified map """

import numpy as np
import re



## Path of CSV file to parse lats and longs from
infile = open('/Users/Matt/Desktop/JULYDATA/julydatawithoutnodes.csv')
## CSV files of all nodes in map
nodesfile = '/Users/Matt/Documents/Programming/pic-math/Discrete Event Simulation/Map/nodes.csv'
## Path of file to write the output nodes to
outfile = open('/Users/Matt/Desktop/JULYDATA/julydataupdated.csv', 'w')
## Columns headers for output
outfile.write('starttime, priority, duration, latitude, longitude, node \n')


## Create node name, latitude, and longitude lists for each entry in infile
nodeid = np.genfromtxt(nodesfile,delimiter=',',usecols=0,skip_header=1)
nodelat = np.genfromtxt(nodesfile,delimiter=',',usecols=1,skip_header=1)
nodelong = np.genfromtxt(nodesfile,delimiter=',',usecols=2,skip_header=1)


"""  Function that finds the closest node based on a latitude and longitude,
    and the lats and longs of each node """

def findnode(lat,long):
    mindist = 1000000                           # Arbitrarily large value for minimum
    for node in range(0,len(nodeid)):           # For all nodes in list,
        dy = lat - nodelat[node]                # Find the x and y distance from node to
        dx = long - nodelong[node]              # given lat and long
        dist = dx * dx + dy * dy    # Total distance (should be square root but for comparison, doesn't matter
        if dist<mindist:                        # Keeps node data only it is the closest node considered so far
            closestnode = nodeid[node]
            mindist = dist
    return int(closestnode)                     # Return number of closest node



infile.readline()

""" Read in each line for the file and parse out each data column.  Find the closest node, then rewrite
    all data to a new file, now with node number in addition to lat and long"""


for line in infile:
    line = line.strip()
    calltype, priority, dispatchtime, calldur, address, latitude, longitude = line.split(',')
    date, time = dispatchtime.split(' ')
    month,day,year = date.split('/')
    hour, mampm = time.split(':')
    minute, am = [mampm[i:i + 2] for i in range(0, len(mampm), 2)]
    if am == 'AM':
        if int(hour) == 12:
            hour = 0
    if am == 'PM':
        if int(hour) != 12:
            hour = int(hour) + 12
    starttime = (int(day)-25)*60*24 + int(hour)*60 + int(minute)
    node = findnode(float(latitude),float(longitude))
    outfile.write('%d, %s, %s, %s, %s, %s \n' % (starttime, priority, calldur, latitude, longitude, node))

outfile.close()
