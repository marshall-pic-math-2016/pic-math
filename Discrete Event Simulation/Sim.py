import simpy
import numpy as np
import SimLib
from SimLib import Car, Call
import sys

"""
sys.argv[x] is the x-th argument given when this script is called

Syntax:

    python Sim.py plan_to_eval number_of_cars

For example, to use the map contained in the badpatroltime_7 folder,
which is a 7-zone, 7-car plan,:

    python GlobalResponse.py badpatroltime_7 7

"""

plan_to_eval = str(sys.argv[1])

#CSV files containing nodes and edges
nodesfile = 'Map/%s/nodes.csv' %(plan_to_eval)               #CSV file containing nodes
edgesfile = 'Map/edges.csv'                  #CSV file containing edges

# File to which response time results are written
rtfile = open('Map/%s/responsetimes.csv' %plan_to_eval, 'w')
rtfile.write('1st, 2nd \n')

# File containing historical crime data to be tested
eventsfile = 'oneweekdata/julydataupdated.csv'



"""
Define list of cars and zone assignments here
Creates the number of cars specified in the script call,
and assigns them based on which number they are.
First car is car0, which is assigned to zone 0
This method only works if there are no cover cars!
"""

cars = []
workloads = []
for i in range(0,int(sys.argv[2])):
    cars.append({'name': i, 'zone': i})
    workloads.append(0)

"""
Speeds used for path-finding algorithm that controls routing
and response times
highspeeds are the higher speeds used for officers responding to calls
lowspeeds are cruising speeds when officers are idle
All in miles per hour.
"""

highspeeds = [25,30,35,40,50,70]            #Higher speed for Dijkstra event routing
lowspeeds = [10,15,20,25,35,65]             #Lower cruising speed

for n in range(0,len(highspeeds)):              #Change speeds to meters/hr
    highspeeds[n] = highspeeds[n]*1609.34
    lowspeeds[n] = lowspeeds[n]*1609.34

graph = SimLib.Graph()                          #Creates graph to append nodes/edges to

"""
Parses out all node and edge data from input map files
Adds this data to the graph of the entire city
Data comes from OpenStreetMap.org
Necessary parameters for nodes are node number, latitude, longitude, and
    patrol zone that node belongs to
Parameters for edges are edge number, the start and end node numbers it connects,
its length for calculation of travel time, the type of road,
and whether it is one-way or not
"""

nodes = np.genfromtxt(nodesfile,delimiter=',',usecols=0,skip_header=1)              #Parses out node data from
lat = np.genfromtxt(nodesfile,delimiter=',',usecols=2, skip_header=1)               #CSV file specified earlier
long = np.genfromtxt(nodesfile,delimiter=',',usecols=1,skip_header=1)
zonenumber = np.genfromtxt(nodesfile,delimiter=',',usecols=3,skip_header=1)

edge_id = np.genfromtxt(edgesfile,delimiter=',',usecols=0,skip_header=1)            #Array of edge names
edge_sn = np.genfromtxt(edgesfile,delimiter=',',usecols=1,skip_header=1)            #Start nodes for each edge
edge_dn = np.genfromtxt(edgesfile,delimiter=',',usecols=2,skip_header=1)            #and destination nodes
edge_length = np.genfromtxt(edgesfile,delimiter=',',usecols=3,skip_header=1)        #Length of edge in meters
edge_road = np.genfromtxt(edgesfile,delimiter=',',usecols=4,skip_header=1)          #
edge_oneway = np.genfromtxt(edgesfile,delimiter=',',usecols=5,skip_header=1)        #One way value

for node in range(0,len(nodes)):                                                   #Add nodes and their zones
    graph.add_node(str(int(nodes[node])),str(int(zonenumber[node])), lat[node], long[node])               #to graph
for n in range(0,len(edge_id)):                                         #Add edges to graph
    lt = edge_length[n]
    ht = edge_length[n]
    lt = edge_length[n] / lowspeeds[int(edge_road[n]-1)]                #And speed of driving
    ht = edge_length[n] / highspeeds[int(edge_road[n]-1)]
    if edge_oneway[n] != 0:                                             #Add one-way and two-way roads
        graph.add_edge(str(int(edge_sn[n])),str(int(edge_dn[n])),lt, ht)
    else:
        graph.add_edge_oneway(str(int(edge_sn[n])),str(int(edge_dn[n])),lt, ht)

"""
Parses out data for call for service events from historical data
Parameters are call start time, measured in minutes from midnight on the first day of the data,
call priority (1-7), duration of call(minutes for which the officer was busy responding),
and the node the call occurred at.
"""


callstarttime = np.genfromtxt(eventsfile,delimiter=',',usecols=0,skip_header=1)
callpri = np.genfromtxt(eventsfile,delimiter=',',usecols=1,skip_header=1)
callduration = np.genfromtxt(eventsfile,delimiter=',',usecols=2,skip_header=1)
callnode = np.genfromtxt(eventsfile,delimiter=',',usecols=5,skip_header=1)




"""
Runs simulation through 25 iterations of the week of data
Creates the simulation environment, then creates all Cars and Calls,
and runs the simulation for the entire week of data, recording all call responses
Cars also keep track of their individual workloads (number of calls they respond to)
for calculation of workload distribution between cars
"""



for n in range(0,25):
    CarEvents = []  # Empty list to store cars

    env = simpy.Environment()  # Create an enviroment for events to take place in

    for carn in cars:                                               #For each car, create a Car event using
        number, zone = int(carn['name']), int(carn['zone'])         # car name and assigned zone from above
        CarEvents.append(Car(env,graph,number,zone))

    for event in range(0,len(callstarttime)):
        starttime,priority,calldur, callloc = int(callstarttime[event]),int(callpri[event]),int(callduration[event]),int(callnode[event])
        env.process(Call(env,graph,CarEvents,starttime,callloc,calldur,priority,rtfile,n))

    env.run(until=10080)

    for c in range(0,len(CarEvents)):
        workloads[c] += CarEvents[c].workload

rtfile.close()



rts1 = np.genfromtxt('Map/%s/responsetimes.csv' %plan_to_eval,delimiter=',',usecols=0,skip_header=1)
rts2 = np.genfromtxt('Map/%s/responsetimes.csv' %plan_to_eval,delimiter=',',usecols=1,skip_header=1)

# Prints average first and second response time (each call requires response from two cars)

print "Average 1st response time: ", np.average(rts1)
print "Average 2nd response time: ", np.average(rts2)

# Prints each car's workload for calculation of variation

for car in range(0,len(CarEvents)):
    print "Car %d (Zone %d) calls:  %d" % (CarEvents[car].number, CarEvents[car].zone, workloads[car])

