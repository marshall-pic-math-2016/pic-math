# Discrete Event Simulation


## Overview

Simulations created for HPD to test the potential success of a police patrol zoning scheme.  
The city is imported as a graph of nodes and edges (intersections and roads, respectively) and simulated cars randomly patrol their assigned zones.  Historical call for service data is imported and cars are programmed to respond as calls come in.  Response time (calculated using Dijkstra's Algorithm) and workload distribution variance (calculated using number of calls each car responds to) are outputs.  

The basic discrete simulation is the file `Sim.py`.

`SimLib.py` contains the classes and functions required for all other programs here.

`ShiftingSim.py` works similarly to `Sim.py`, but evaluates the morning, evening, and midnight shifts separately, so different zone plans can be assigned to each (for example, testing a plan that uses 7 zones during morning and midnight and 10 zones during evening.

`GlobalResponse.py` calculates average response time for cars to converge on a global high priority event (shooting, armed robbery, etc).  

## Maps

Maps must be created using GIS software and Excel.  The `Map/` folder contains many examples of this, including all of the node and edge data from Huntington WV.  This data is downloaded from OpenStreetMap.org and parsed using `osm4routing`, with unnecessary parameters removed.  The base map file is `map.osm`, while edges and nodes have been parsed out in `edges.csv` and `nodes.csv`. 

The GIS shapefiles for the graph of all nodes in the city is contained in the `Shape/` folder.  To create maps, this shapefile should be the starting point.  

Open up the shapefile in GIS, and use the polygon tool to draw potential zone plans over the map of nodes.  Then copy all of the nodes contained in each zone and paste them as a new shapefile.  Observe in the map folders in the `Map/` directory how each zone has its own folder with its shapefile.  

The `nodes.csv` file in each map folder is the end result that is imported into the simulations.  For each of the new shapefiles you've created for a map, open the attribute table and copy the data from each into Excel, adding a column for zone number.  You will end up with another list of all nodes in the city, but now with a zone attribute that is necessary for all simulations.  See all of the example maps that were tested in the `Maps/` directory to see how files should be organized for testing.  

## Running Simulations

Each type of simulation should be called from the command line.  The parameters for each are as follows:

#### Sim.py
To run the Sim.py code on a zone plan, the required parameters are the zone plan to evaluate and the number of cars.  The syntax is 

`python Sim.py plan_to_eval number_of_cars`

For example, to run this simulation on the map `plan6_7z` in the `Maps/` directory, type

`python Sim.py plan6_7z 7`

#### ShiftingSim.py
This simulation tests only one of the three shifts for the entire week of data.  This can be used either for testing different numbers of zones for each shift (7-10-7 scheme), or to simply speed up the full evaluation of a plan over all three shifts for a week.

Syntax now requires the shift name to be specified, either `am`, `ev`, or `pm`.  Structure is

`python ShiftingSim.py plan_to_eval number_of_cars shift`

For example, to simulate the entirety of the 7-10-7 scheme (7 cars for am and midnight shifts, 10 for evening), the following commands would be used:

`python ShiftingSim.py plan6_7z 7 am`
`python ShiftingSim.py plan9_10z 10 ev`
`python ShiftingSim.py plan6_7z 7 pm`

#### GlobalResponse.py
Syntax is identical to that of Sim.py:

`python Sim.py plan_to_eval number_of_cars`

#### Contact
Please contact Matthew Haldeman at haldeman3@marshall.edu or matthaldeman95@gmail.com for questions.


