from collections import defaultdict, deque
import numpy as np
import random
import simpy

random.seed(1)

"""
SimLib.py contains all of the classes and function required to
run all other code
"""

"""
The Graph class contains the map of the city
It stores all node and edge location

Graph, dijkstra, and shortest_path codes found here:
    http://zurb.com/forrst/posts/Dijkstras_algorithm_in_Python-B4U
"""

class Graph:                                            #Define a class called Graph
    def __init__(self):
        self.nodes = set()
        self.edges = defaultdict(list)
        self.distances = {}
        self.lowtime = {}
        self.hightime={}
        self.zone={}

    def add_node(self, value, zone, lat, long):     # Defining node attributes
        self.nodes.add(value)
        self.zone[value] = zone

    def add_edge(self, from_node, to_node, low_time, high_time):   #Add a two way edge between connected nodes
        self.edges[from_node].append(to_node)
        self.edges[to_node].append(from_node)
        self.lowtime[(to_node, from_node)] = low_time           # Assign low and high travel times
        self.lowtime[(from_node, to_node)] = low_time
        self.hightime[(to_node, from_node)] = high_time
        self.hightime[(from_node, to_node)] = high_time

    def add_edge_oneway(self, from_node, to_node, low_time, high_time):  #Add a one-way edge
        self.edges[from_node].append(to_node)
        self.lowtime[(from_node, to_node)] = low_time
        self.hightime[(from_node, to_node)] = high_time


    """
    Dijkstra's  Algorithm is used for routing officers to crimes via the fastest route
    """


    def dijkstra(graph, initial):
        visited = {initial:0}
        path = {}
        nodes = set(graph.nodes)
        while nodes:
            min_node = None
            for node in nodes:
                if node in visited:
                    if min_node is None:
                        min_node = node
                    elif visited[node] < visited[min_node]:
                        min_node = node
            if min_node is None:
                break
            nodes.remove(min_node)
            current_weight = visited[min_node]
            for edge in graph.edges[min_node]:
                weight = current_weight + graph.hightime[(min_node,edge)]
                if edge not in visited or weight < visited[edge]:
                    visited[edge] = weight
                    path[edge] = min_node

        return visited, path


#Finds the shortest path between two points using Dijkstra, returns the total travel time

def shortest_path(graph, origin, destination):
    origin = str(origin)
    destination = str(destination)
    visited, paths = Graph.dijkstra(graph, str(origin))
    full_path = deque()
    if origin == destination:
        return 0
    try:
        _destination = paths[str(destination)]
        while _destination != origin:
            full_path.appendleft(_destination)
            _destination = paths[_destination]
        full_path.appendleft(origin)
        full_path.append(destination)
        return visited[destination]  ###list(full_path) - can return this too
    except KeyError:
        print "Check nodes ", origin, destination



#Returns a random node connected to the start node that is inside the specified zone

def random_connected_node(graph, startnode, zone):                              #for later debugging
    possnodes = []                                      #Empty list of possible nodes
    for node in graph.edges[startnode]:                 #Considers all connected nodes
        if int(graph.zone[str(int(node))]) == zone:     #Appends only nodes in specified zone to possible nodes list
            possnodes.append(node)
    dest_node_number = random.randint(0,len(possnodes)-1)   #chooses a random connected node in possible nodes
    dest_node = possnodes[int(dest_node_number)]
    time = graph.lowtime[(startnode, dest_node)]*60
    return dest_node, time                                  #Returns node id and travel time


# Generates a completely random node in the specified zone (not based on edges)
def random_node_in_zone(graph, zone):
    possnodes = []
    for node in graph.nodes:
        if int(graph.zone[str(int(node))]) == zone:         #Creates list of possible nodes in the zone
            possnodes.append(node)
    return int(possnodes[random.randint(0,len(possnodes)-1)])      #Chooses a random node

"""
Car class
"""


class Car(object):                   #Car process

    def __init__(self, env, graph, number, zone):
        self.env = env                                      #Reference to environment all cars belong to
        self.action = env.process(self.patrol(graph))       #Creates patrol action
        self.status = 0                                     #Status of car.  0 = available, 1 = at call and unavailable
        self.number = number                                #Car name
        self.zone = zone                                    #Zone assigned to car
        self.workload = 0


    def patrol(self,graph):
        while True:
            if self.env.now == 0:                            #Assign random start node at beginning
                self.currentnode = random_node_in_zone(graph,self.zone)
            #print('Car %d leaving node %d at time %d' % (self.number, int(self.currentnode), self.env.now))
            #self.file.write('%s, %s, %s, %s, %s \n' % (self.env.now, self.status, self.currentnode, graph.lat[str(self.currentnode)], graph.long[str(self.currentnode)]))
            try:
                try:
                    dest_node, time = random_connected_node(graph,str(self.currentnode),self.zone)    #Returns a random connected node
                except KeyError:
                    dest_node = int(random_node_in_zone(graph,self.zone))    #If there are no connected nodes, randomly choose a new node in zone to restart from
                    time = 0
                except ValueError:
                    dest_node = int(random_node_in_zone(graph,self.zone))  # If there are no connected nodes, randomly choose a new node in zone to restart from
                    time = 0
                #print('Car %d travelling to node %s with travel time %s' %(self.number, dest_node, str(time)))
                yield self.env.timeout(time)                         #Timeout for duration of travel time
                self.currentnode = dest_node             #Set new node as start node for new travel path
                #self.file.write('%s, %s, %s, %s, %s \n' % (self.env.now, self.status, self.currentnode, graph.lat[str(self.currentnode)], graph.long[str(self.currentnode)]))
            except simpy.Interrupt as interrupt:
                self.action = self.env.process(self.to(interrupt,graph))
                yield self.action




    def to(self,interrupt, graph):
        try:
            self.workload = self.workload + 1
            inttime, callloc, calltime, priority = interrupt.cause.split(',')  # Travel time, location, and call time
            self.status = int(priority)  # Car status set to priority of call it is responding to
            inttime = float(inttime.strip())
            yield self.env.timeout(float(inttime))  # Timeout for duration of travel to call
            self.currentnode = int(callloc)  # Car is now at call
            #print "Car %d arriving at call at node %s at %s" % (self.number, self.currentnode, self.env.now)
            # self.file.write('%s, %s, %s, %s, %s \n' % (self.env.now, self.status, self.currentnode, graph.lat[str(self.currentnode)], graph.long[str(self.currentnode)]))
            yield self.env.timeout(int(calltime))  # Timeout for duration of call
            dest_node, time = self.returnzone(graph,self.currentnode,self.zone)
            #print "Car %d resuming travel from node %s at time %s" % (self.number, self.currentnode, self.env.now)
            # print('Car %d travelling to node %s with travel time %s' %(self.number, dest_node, str(time)))
            yield self.env.timeout(time)  # timeout for travel time to new node
            self.currentnode = dest_node  # Car is now at this node, restarts random travel from here
            self.status = 0  # Car available again
            # self.file.write('%s, %s, %s, %s, %s \n' % (self.env.now, self.status, self.currentnode, graph.lat[str(self.currentnode)], graph.long[str(self.currentnode)]))
            self.action = self.env.process(self.patrol(graph))
            yield self.action
        except simpy.Interrupt as interrupt:
            self.action = self.env.process(self.to(interrupt,graph))
            yield self.action


    def returnzone(self,graph,currentnode,zone):
        while True:
            try:
                dest_node = random_node_in_zone(graph, zone)  # Pick a new node to return to in zone to resume patrol
                time = shortest_path(graph, currentnode, dest_node) * 60
                return int(dest_node), int(time)
            except:
                continue


"""
Call for service class
"""

def Call(env, graph, CarEvents, starttime, callloc, calldur, priority, rts, it):
    yield env.timeout(starttime)                            #Waits until specified time for start of call
    print "Iteration: %s, call at time %s" % (it, starttime)
    mintime = 1000                                          #Large variable so minimum can be found
    mintime2 = 1000
    availablecars = 0
    for carn in CarEvents:                              #Find number of cars that are available
        #print carn.number, carn.status
        if carn.status == 0:
            availablecars = availablecars + 1
    #print availablecars, "cars available"


    if availablecars == 0:
        maxpri = 1
        maxpri2 = 1
        for carn in CarEvents:
            if maxpri == 1:
                maxpri = carn.status
            else:
                if carn.status > maxpri:
                    maxpri2 = maxpri
                    maxpri = carn.status
                elif carn.status > maxpri2:
                    maxpri2 = carn.status
        #print ("Lowest Priorities: %s, %s" % (maxpri, maxpri2))

        for carn in CarEvents:
            if carn.status == 0 or carn.status == maxpri or carn.status == maxpri2:
                try:
                    time = shortest_path(graph, carn.currentnode, callloc) * 60
                except TypeError: continue
                #print carn.number, time
                if mintime == 1000:
                    mintime, closestcar = time, carn
                else:
                    if time < mintime:
                        mintime2, closestcar2 = mintime, closestcar
                        mintime, closestcar = time, carn
                    elif time < mintime2:
                        mintime2, closestcar2 = time, carn


    if availablecars == 1:                                          #If only one car is available,
        maxpri = 1
        for carn in CarEvents:                                      #Considers each patrol car
            if carn.status == 1:
                maxpri = carn.status
            else:
                if carn.status > maxpri:
                    maxpri = carn.status
        #print "Lowest priority: %s" % maxpri
        for carn in CarEvents:
            if carn.status == 0 or carn.status == maxpri:
                try:
                    time = shortest_path(graph, carn.currentnode, callloc)*60
                except TypeError: continue
                #print carn.number, time
                if mintime == 1000:
                    mintime, closestcar = time, carn
                else:
                    if time < mintime:
                        mintime2, closestcar2 = mintime, closestcar
                        mintime, closestcar = time, carn
                    elif time < mintime2:
                        mintime2, closestcar2 = time, carn




    if availablecars > 1:                                             #If there are greater than 1 available cars,
        for carn in CarEvents:
            if carn.status == 0:
                try:
                    time = shortest_path(graph,carn.currentnode, callloc)*60  #Response time
                except TypeError: continue
                #print carn.number, time
                if mintime == 1000:                             #If first iteration, make first car considered the closest car
                    mintime = time
                    closestcar = carn
                else:
                    if time < mintime:                          #Finds closest 2 cars out of all available cars
                        mintime2 = mintime
                        closestcar2 = closestcar
                        mintime = time
                        closestcar = carn
                    elif time < mintime2:
                        mintime2 = time
                        closestcar2 = carn


    #print "Cars dispatched:  Car %d, %f    Car %d, %f \n \n" %(closestcar.number, mintime, closestcar2.number, mintime2)
    rts.write('%s, %s \n' % (str(mintime), str(mintime2)))

    closestcar.action.interrupt("%s, %s, %d, %d" % (mintime2,callloc,calldur, priority))       #Interrupts closest two cars
    try:
        closestcar2.action.interrupt("%s, %s, %d, %d" % (mintime2,callloc,calldur, priority))
    except:
        return