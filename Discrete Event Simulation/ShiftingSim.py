import simpy
import numpy as np
import SimLib
from SimLib import Car, Call
import sys
                                            #Speeds in miles per hour
highspeeds = [25,30,35,40,50,70]            #Higher speed for Dijkstra event routing
lowspeeds = [10,15,20,25,35,65]             #Lower cruising speed


plan_to_eval = str(sys.argv[1])

shift = str(sys.argv[3])


"""
if shift == 'am':
    nodesfile = 'Map/%s/shifta/nodes.csv' %(plan_to_eval)
if shift =='ev':
    nodesfile = 'Map/%s/shiftb/nodes.csv' %(plan_to_eval)
if shift == 'pm':
    nodesfile = 'Map/%s/shiftc/nodes.csv' %(plan_to_eval)
"""

nodesfile = 'Map/%s/nodes.csv' %(plan_to_eval)
edgesfile = 'Map/edges.csv'                  #CSV file containing edges
eventsfile = 'oneweekdata/julydataupdated.csv'
rtfile = open('Map/%s/responsetimes%s.csv' % (plan_to_eval, shift), 'w')
cars = []
workloads = []
for i in range(0,int(sys.argv[2])):
    cars.append({'name': i, 'zone': i})
    workloads.append(0)


for n in range(0,len(highspeeds)):              #Change speeds to meters/hr
    highspeeds[n] = highspeeds[n]*1609.34
    lowspeeds[n] = lowspeeds[n]*1609.34

graph = SimLib.Graph()                          #Creates graph to append nodes/edges to

nodes = np.genfromtxt(nodesfile,delimiter=',',usecols=0,skip_header=1)              #Parses out node data from
lat = np.genfromtxt(nodesfile,delimiter=',',usecols=2, skip_header=1)               #CSV file specified earlier
long = np.genfromtxt(nodesfile,delimiter=',',usecols=1,skip_header=1)
zonenumber = np.genfromtxt(nodesfile,delimiter=',',usecols=3,skip_header=1)

edge_id = np.genfromtxt(edgesfile,delimiter=',',usecols=0,skip_header=1)            #Array of edge names
edge_sn = np.genfromtxt(edgesfile,delimiter=',',usecols=1,skip_header=1)            #Start nodes for each edge
edge_dn = np.genfromtxt(edgesfile,delimiter=',',usecols=2,skip_header=1)            #and destination nodes
edge_length = np.genfromtxt(edgesfile,delimiter=',',usecols=3,skip_header=1)        #Length of edge in meters
edge_road = np.genfromtxt(edgesfile,delimiter=',',usecols=4,skip_header=1)          #
edge_oneway = np.genfromtxt(edgesfile,delimiter=',',usecols=5,skip_header=1)        #One way value

callstarttime = np.genfromtxt(eventsfile,delimiter=',',usecols=0,skip_header=1)
callpri = np.genfromtxt(eventsfile,delimiter=',',usecols=1,skip_header=1)
callduration = np.genfromtxt(eventsfile,delimiter=',',usecols=2,skip_header=1)
callnode = np.genfromtxt(eventsfile,delimiter=',',usecols=5,skip_header=1)

for node in range(0,len(nodes)):                                                   #Add nodes and their zones
    graph.add_node(str(int(nodes[node])),str(int(zonenumber[node])), lat[node], long[node])               #to graph
for n in range(0,len(edge_id)):                                         #Add edges to graph
    lt = edge_length[n]
    ht = edge_length[n]
    lt = edge_length[n] / lowspeeds[int(edge_road[n]-1)]                #And speed of driving
    ht = edge_length[n] / highspeeds[int(edge_road[n]-1)]
    if edge_oneway[n] != 0:                                             #Add one-way and two-way roads
        graph.add_edge(str(int(edge_sn[n])),str(int(edge_dn[n])),lt, ht)
    else:
        graph.add_edge_oneway(str(int(edge_sn[n])),str(int(edge_dn[n])),lt, ht)



rtfile.write('1st, 2nd \n')


for n in range(0,25):
    CarEvents = []  # Empty list to store cars

    env = simpy.Environment()  # Create an enviroment for events to take place in

    for carn in cars:  # For each car, create a Car event using
        number, zone = int(carn['name']), int(carn['zone'])  # car name and assigned zone from above
        CarEvents.append(Car(env, graph, number, zone))


    for event in range(0, len(callstarttime)):
        starttime, priority, calldur, callloc = int(callstarttime[event]), int(callpri[event]), int(callduration[event]), int(callnode[event])
        if shift == 'am':
                if starttime%1440 >= 360 and starttime%1440 < 840:
                    env.process(Call(env, graph, CarEvents, starttime, callloc, calldur, priority, rtfile,n))
        if shift =='ev':
                if starttime%1440 >= 840 and starttime%1440 < 1320:
                    env.process(Call(env, graph, CarEvents, starttime, callloc, calldur, priority, rtfile,n))
        if shift =='pm':
                if starttime%1440 >= 1320 or starttime%1440 < 360:
                    env.process(Call(env, graph, CarEvents, starttime, callloc, calldur, priority, rtfile,n))


    env.run(until=10080)

    for c in range(0,len(CarEvents)):
        workloads[c] += CarEvents[c].workload

rtfile.close()



rts1 = np.genfromtxt('Map/%s/responsetimes%s.csv' %(plan_to_eval,shift),delimiter=',',usecols=0,skip_header=1)
rts2 = np.genfromtxt('Map/%s/responsetimes%s.csv' %(plan_to_eval,shift),delimiter=',',usecols=1,skip_header=1)

print "Average 1st response time: ", np.average(rts1)
print "Average 2nd response time: ", np.average(rts2)

for car in range(0,len(CarEvents)):
    print "Car %d (Zone %d) calls:  %d" % (CarEvents[car].number, CarEvents[car].zone, workloads[car])
