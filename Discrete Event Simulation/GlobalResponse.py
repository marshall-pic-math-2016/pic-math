import numpy as np
import SimLib
import random
import sys
random.seed(1)
"""
sys.argv[x] is the x-th argument given when this script is called

Syntax:

    python GlobalResponse.py plan_to_eval number_of_cars

For example, to use the map contained in the badpatroltime_7 folder,
which is a 7-zone, 7-car plan,:

    python GlobalResponse.py badpatroltime_7 7

"""

plan_to_eval = str(sys.argv[1])      #Name of folder containing current zone plan

nodesfile = 'Map/%s/nodes.csv' %(plan_to_eval)

"""
Define list of cars and zone assignments here
Creates the number of cars specified in the script call,
and assigns them based on which number they are.
First car is car0, which is assigned to zone 0
This method only works if there are no cover cars!
"""

cars = []
for i in range(0,int(sys.argv[2])):
    cars.append(dict(name=i,zone=i))


"""
List of events.  Here, four crime hotspots are used
in locations where a global response would be expected
(All cars converge on one spot for an extremely high priority call)
for example, Marcum Terrace, etc
"""
calls=['154812912','154787088','154855326','154793466']

#CSV file containing edges
edgesfile = 'Map/edges.csv'
# File that will hold the output response times
rtfile = open('Map/%s/globalresponsetimes.csv' % (plan_to_eval), 'w')

"""
Speeds used for path-finding algorithm that controls routing
and response times
highspeeds are the higher speeds used for officers responding to calls
lowspeeds are cruising speeds when officers are idle
All in miles per hour.
"""
highspeeds = [25,30,35,40,50,70]            #Higher speed for Dijkstra event routing
lowspeeds = [10,15,20,25,35,65]             #Lower cruising speed
for n in range(0,len(highspeeds)):              #Change speeds to meters/hr
    highspeeds[n] = highspeeds[n]*1609.34
    lowspeeds[n] = lowspeeds[n]*1609.34

graph = SimLib.Graph()                          #Creates graph to append nodes/edges to

"""
Parses out all node and edge data from input map files
Adds this data to the graph of the entire city
Data comes from OpenStreetMap.org
Necessary parameters for nodes are node number, latitude, longitude, and
    patrol zone that node belongs to
Parameters for edges are edge number, the start and end node numbers it connects,
its length for calculation of travel time, the type of road,
and whether it is one-way or not
"""


nodes = np.genfromtxt(nodesfile,delimiter=',',usecols=0,skip_header=1)              #Parses out node data from
lat = np.genfromtxt(nodesfile,delimiter=',',usecols=2, skip_header=1)               #CSV file specified earlier
long = np.genfromtxt(nodesfile,delimiter=',',usecols=1,skip_header=1)
zonenumber = np.genfromtxt(nodesfile,delimiter=',',usecols=3,skip_header=1)

edge_id = np.genfromtxt(edgesfile,delimiter=',',usecols=0,skip_header=1)            #Array of edge names
edge_sn = np.genfromtxt(edgesfile,delimiter=',',usecols=1,skip_header=1)            #Start nodes for each edge
edge_dn = np.genfromtxt(edgesfile,delimiter=',',usecols=2,skip_header=1)            #and destination nodes
edge_length = np.genfromtxt(edgesfile,delimiter=',',usecols=3,skip_header=1)        #Length of edge in meters
edge_road = np.genfromtxt(edgesfile,delimiter=',',usecols=4,skip_header=1)          #
edge_oneway = np.genfromtxt(edgesfile,delimiter=',',usecols=5,skip_header=1)        #One way value

for node in range(0,len(nodes)):                                                   #Add nodes and their zones
    graph.add_node(str(int(nodes[node])),str(int(zonenumber[node])), lat[node], long[node])               #to graph
for n in range(0,len(edge_id)):                                         #Add edges to graph
    lt = edge_length[n] / lowspeeds[int(edge_road[n]-1)]                #And speed of driving
    ht = edge_length[n] / highspeeds[int(edge_road[n]-1)]
    if edge_oneway[n] != 0:                                             #Add one-way and two-way roads
        graph.add_edge(str(int(edge_sn[n])),str(int(edge_dn[n])),lt, ht)
    else:
        graph.add_edge_oneway(str(int(edge_sn[n])),str(int(edge_dn[n])),lt, ht)

"""
Main loop.
For 1000 iterations, each car picks a random node in their zone
Then each car's travel time to the hot spots are calculated and recorded
"""

for n in range(0,1000):
    print "Iteration", n
    for call in calls:
        for carn in cars:
            try:
                origin = SimLib.random_node_in_zone(graph, int(carn['zone']))
                time = SimLib.shortest_path(graph,origin,call)*60
                rtfile.write('%s \n' % time)
            except TypeError:
                break

rtfile.close()          # Output file closed

"""
Reads in the output file from before and calculates the average response time
Prints this to terminal output
"""

rts = np.genfromtxt('Map/%s/globalresponsetimes.csv' % (plan_to_test),delimiter=',',usecols=0)
avgrt = np.average(rts)
print avgrt