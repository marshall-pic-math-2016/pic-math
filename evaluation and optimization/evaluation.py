import sys
import os
import fiona
from shapely.geometry import shape, Point, Polygon, mapping
from shapely.ops import cascaded_union
from shapely.prepared import prep
import datetime
import statistics

class Zone:
    def __init__(self, polygon):
        self.polygon = polygon
        self.atoms = []
        self.point_count = 0
        self.number = 0
        self.workload_score = 0
        self.boundaries = []

class Atom:
    def __init__(self, polygon):
        self.polygon = polygon
        self.zone = None
        self.point_count = 0
        self.center = None
        self.boundary = False
        self.neighbors = {
            'north': None,
            'east': None,
            'south': None,
            'west': None
        }

# READ SHAPEFILES

def evaluate_map(zones, points, viaducts, hotspots):
    """
        Lower scores correspond to better maps.
    """

    weights = {
        'response time': 0.0104,
        'workload distribution': 0.0002342,
        'patrol time': 0.009718,
        'viaducts': 0.004013,
        'global response time': 0.975631697
    }

    workload_score = score_workload(zones)
    resp_time_score = score_response_time(zones)
    patrol_time_score = score_patrol_time(zones)
    viaduct_score = score_viaducts(zones, viaducts)
    global_response_time = score_global_response_time(zones,hotspots)

    print "SCORES:"
    print "                  Workload: {}".format(workload_score)
    print "             Response Time: {}".format(resp_time_score)
    print "               Patrol Time: {}".format(patrol_time_score)
    print "                  Viaducts: {}".format(viaduct_score)
    print "Global Response Time Score: {}".format(global_response_time)

    score = \
        workload_score**weights['workload distribution'] * \
        resp_time_score**weights['response time'] * \
        patrol_time_score**weights['patrol time'] * \
        viaduct_score**weights['viaducts'] * \
        global_response_time**weights['global response time']

    print "OVERALL: {}".format(score)

    return score

#
# SCORING FUNCTIONS
#

def score_viaducts(zones, viaducts):
    """
    We wish to penalize those zone plans that contain viaducts.
    Around each viaduct, a delta-ball is drawn (for a given delta).
    If that ball does NOT intersect the boundary of a zone, then
    the zone is penalized.

    Here, a minimal score is good, so penalization corresponds to an increased score.
    """
    buffer_dist = 160 # this is in meters (160m is about one block)
    count = 0

    zone_boundaries = [(zone.polygon.boundary, zone.polygon) for zone in zones]

    # be sure to take weights into account eventually
    for viaduct_point, weight in viaducts:
        # buffer
        d_ball = viaduct_point.buffer(buffer_dist)
        for bound, zone in zone_boundaries:
            if d_ball.intersects(bound) or not viaduct_point.within(zone):
                pass
            else:
                count += 1

    v_score = count / float(len(viaducts))
    return 1 + v_score

# on the order of 2 thousandTHS
def score_patrol_time(zones):
    count = 0
    for zone in zones:
        zone_score = zone.polygon.boundary.length/zone.polygon.area
        count += zone_score
    pt_score = count / len(zones)
    return pt_score

def score_response_time(zones):
    count = 0
    for zone in zones:
        count += response_time_score_for_zone(zone)
    return count

def score_workload(zones):
    stand_dev = statistics.stdev([workload_score_for_zone(z) for z in zones])
    return 1 + stand_dev

def score_global_response_time(zones,hotspots):
    total = 0
    for spot in hotspots:
        count = 0
        for zone in zones:
            count += zone.polygon.representative_point().distance(spot)
        total += count/len(zones)

    return total/len(hotspots)

def score_traffic(zones,traffic):
    score = 0

    for light, count in traffic:
        ball = light.buffer(320) # using the current projection, 320m ~ 2 blocks

        for zone in zones:
            int_area = ball.intersection(zone.polygon).area
            zone_area = zone.polygon.area
            score += int_area/zone_area*count

    return score

def read_shapes(from_file):
    """
    This function uses fiona to read vector data
    from geometric files. Each entry is then
    converted to a shape using shapely.

    Returns a list of shapes.
    """
    items = []

    with fiona.open(from_file) as shapes:
        for item in shapes:
            items.append(shape(item['geometry']))
            # the shape() coerces the geometry to a shapely shape
    return items

#
# READ/WRITE
#

def read_zones(from_file):
    """
    This is literally the same thing as read_shapes,
    but it also encapsulates the zone in a Zone
    class with extra properties to manage points
    and the like.
    """
    # TODO: It would be very nice to have a Zone Number attribute in every
    # zone shapefile that could then be read from those files, resulting
    # in a more consitent experience.
    items = []

    with fiona.open(from_file) as shapes:
        for index, item in enumerate(shapes, start=1):
            z = Zone(shape(item['geometry']))
            z.number = index
            items.append(z)
    return items

def read_traffic(from_file):
    items = []

    with fiona.open(from_file) as shapes:
        for item in shapes:
            items.append((shape(item['geometry']), item['properties']['COUNT']))

    return items

def save_polygons(file, polygons):
    output_schema = {
        'geometry': 'Polygon',
        'properties': {'id': 'int'},
    }

    path = os.path.split(file)[0]
    filename = os.path.basename(os.path.splitext(file)[0])
    ext = '.shp'

    if not os.path.exists(path):
        os.makedirs(path)

    final_path = os.path.join(path, filename + ext)

    with fiona.open(
            final_path,
            'w',
            driver='ESRI Shapefile',
            crs={'init': u'epsg:26917'},
            schema=output_schema) as c:
        for x in range(len(polygons)):
            c.write({
                'geometry': mapping(polygons[x]),
                'properties': {'id': x},
            })

    return True

#
# HELPER FUNCTIONS
#

def create_atoms(for_zones, with_granularity):
    """
    creates square atoms covering the region

    granularity divides the width of the region
    (so granularity of 5 gives 5 zones horizontally)
    """
    atoms = []
    rows = []
    zone_zero = Zone(Point(0,0))

    bounds = cascaded_union([zone.polygon for zone in for_zones]).bounds
    left_bound = bounds[0]
    bottom_bound = bounds[1]
    right_bound = bounds[2]
    top_bound = bounds[3]

    side_width = int((right_bound - left_bound)/with_granularity)

    left_start = left_bound
    top_start = top_bound
    i = 0
    while top_start > bottom_bound:
        rows.append([])
        for x in range(with_granularity):
            a = Atom(Polygon( [ (left_start, top_start), (left_start + side_width, top_start), (left_start + side_width, top_start - side_width), (left_start, top_start - side_width), (left_start, top_start)] ))
            a.center = a.polygon.representative_point()
            z = determine_zone(for_zones, a.polygon)
            if z:
                a.zone = z
                z.atoms.append(a)
            else:
                a.zone = zone_zero
                zone_zero.atoms.append(a)
            atoms.append(a)
            left_start = left_start + side_width
            rows[i].append(a)
        left_start = left_bound
        top_start = top_start - side_width
        i += 1
    determine_neighbors(rows,zone_zero)
    determine_boundaries(atoms)
    return atoms

def determine_neighbors(rows,zone_zero):
    # here we loop through the stuff, determining neighbors and such
    for i, row in enumerate(rows):
        for k, atom in enumerate(row):
            if k - 1 >= 0:
                if row[k - 1].zone != zone_zero:
                    atom.neighbors['west'] = row[k - 1]
            if k + 1 < len(row):
                if row[k + 1].zone != zone_zero:
                    atom.neighbors['east'] = row[k + 1]
            if i - 1 >= 0:
                if rows[i - 1][k].zone != zone_zero:
                    atom.neighbors['north'] = rows[i - 1][k]
            if i + 1 < len(rows):
                if rows[i + 1][k].zone != zone_zero:
                    atom.neighbors['south'] = rows[i + 1][k]
    return

def determine_boundaries(atoms):
    # here we loop through the atoms and determine
    # if an atom is a boundary atom based on its neighbors
    #
    # atoms on the exterior of the city are NOT boundary atoms
    for atom in atoms:
        for dir in ['west', 'north', 'east', 'south']:
            if atom.neighbors[dir] != None:
                if atom.neighbors[dir].zone != atom.zone:
                    atom.boundary = True
                    atom.zone.boundaries.append(atom)
    return

def determine_zone(for_zones, poly):
    for zone in for_zones:
        if poly.intersects(zone.polygon) == True:
            return zone
    return

def count(atoms,points):
    for point in points:
        for atom in atoms:
            if atom.polygon.contains(point) == True:
                atom.point_count += 1
                atom.zone.point_count += 1
                break

def workload_score_for_zone(zone):
    zone.workload_score = zone.point_count
    return zone.workload_score

def response_time_score_for_zone(zone):
    score = 0
    zone_center = zone.polygon.representative_point()
    for atom in zone.atoms:
        dist = atom.center.distance(zone_center)
        score += atom.point_count*dist
    zone.response_time_score = score
    return score