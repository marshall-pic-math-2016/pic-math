# Evaluation and Optimization

Code for evaluation and optimization is split into two python files called,
appropriately and respectively, `evaluation.py` and `optimization.py`.
The dependencies for these files can be installed by running `pip-sync
requirements.txt` in this directory. (If you are running Windows, you may
need to manually install `Fiona` and `Shapely`, libraries for reading/writing
and manipulating ESRI shapefiles. These binaries and instructions for installing
them can be found at the [Python Package Index](https://pypi.python.org/pypi/Shapely).)

## Evaluation

This file contains methods for reading and writing ESRI shapefiles and for
producing a fitness function score for each zone map.

### Reading a Zone Plan

To read a zone plan into memory, use the `read_zones(from_file)` command,
where `from_file` is the path to the ESRI shapefile containing the zones
comprising a zone plan.

For example, suppose you have an ESRI shapefile in this folder called
`zones.shp`. Then you would read a zone plan into memory by importing
`evaluation.py` and running:

```python
zones = read_zones('zones.shp')
```

This results in a list called `zones` containing a `Zone` object for each
of the polygons in `zones.shp`. The `Zone` object serves as a wrapper to
keep track of things such as point counts, boundaries, and more.

### Reading Crime Data and Hotspots

Because zones are the only object requiring a wrapper, all other data can
be imported using the `read_shapes(from_file)` command.

### Producing a Fitness Score

To produce a fitness function score, one uses the

```python
evaluate_map(zones, points, viaducts, hotspots)
```

command, where `zones` is a list of `Zone` objects,
`points` is a list of points representing historical crime data, `vidaucts` is
a list of tuples including both a point representing the location of a viaduct
and a weight, and `hotspots` is a list of points representing the locations
of historical crime hotspots.

Suppose you have the files `zones.shp`, `crimeloc.shp`, `viaducts.shp`, and
`hotspots.shp` which contain a list with each zone, a list of points
representing the location of crimes, a list of points representing the
locations of viaducts, and a list of points representing crime hotspots,
respectively.

```python
import evaluation

zones = read_zones('zones.shp')
points = read_shapes('crimeloc.shp')
viaducts = read_shapes('viaducts.shp')
# Now we assign a weight of 1 to each viaduct
viaducts = [(v,1) for v in viaducts]
hotspots = read_shapes('hotspots.shp')

score = evaluate_map(zones, points, viaducts, hotspots)
```

Then `score` will be a float representing the score of the map.

#### On Viaduct Weights

Each viaduct must be assigned a relative weight. These weights can be any
float, but it's easier to stick to integers. There is no default, but a good
default is to assign `1` to every viaduct. Higher weights should be given to
viaducts that absolutely should not be contained within a zone.

A list of viaducts should actually be a list of tuples, where the first part of
the tuple is a `Shapely` point object representing the location of the viaduct
and the second part is a float representing the weight.

If you want to assign the same weight to every viaduct, use a list
comprehension after reading the list of points:

```python
viaducts = read_shapes('viaducts.shp')
viaducts = [(v,1) for v in viaducts]
```

## Optimization

Existing maps can be optimized by a method of gradient descent.
This is done using the

```python
wiggle_it(zones,points,viaducts,hotspots)
```

command, where all of the variables are defined as they were for the
`evaluate_map` command. Optimization works by dividing a city into square
"atoms." Each atom on the border between zones is switched to a neighboring
zone and the fitness score (as defined in `evaluation.py`) is recalculated.
Those changes that improve the overall score are kept, and those that do not
are discarded.

The file `optimization.py` is dependent on `evaluation.py` and will import it
automatically, provided the files are both in the same folder.