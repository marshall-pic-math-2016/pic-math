"""
    This is a project.

    TODO: need to export/import with full information
"""
from evaluation import *

def wiggle_it(zones,points,viaducts,hotspots):
    # wiggles exactly one atom

    for zone in zones:
        zone.polygon = zone_polygon(zone)

    # create boundary list
    boundaries = []
    for zone in zones:
        for boundary in zone.boundaries:
              boundaries.append(boundary)

    temp_change = {
        'atom': None,
        'new zone': None,
        'old zone': None,
        'score': evaluate_map(zones, points,viaducts,hotspots)
    }

    for atom in boundaries:
        for key, neighbor in atom.neighbors.iteritems():
            if neighbor and neighbor.zone != atom.zone:
                # switch the atom
                old_zone = atom.zone
                switch_atom_zone(atom,neighbor.zone)

                score = evaluate_map(zones,points,viaducts,hotspots)

                if (score < temp_change['score']):
                    temp_change['atom'] = atom
                    temp_change['new zone'] = neighbor.zone
                    temp_change['old zone'] = atom.zone
                    temp_change['score'] = score

                switch_atom_zone(atom,old_zone)

    if temp_change['atom'] == None:
        return (zones, None, None, None)


    switch_atom_zone(temp_change['atom'],temp_change['new zone'])

    atoms = []
    for zone in zones:
        zone.boundaries = []
        for atom in zone.atoms:
            atom.boundary = False
            atoms.append(atom)

    determine_boundaries(atoms)

    return (zones, temp_change['atom'], temp_change['new zone'], temp_change['score'])

def switch_atom_zone(atom,zone):
    """
        Changes the zone designation of an atom and tidies up all
        of the necessary dependencies.
    """
    old_zone = atom.zone

    zone.boundaries.append(atom)
    zone.atoms.append(atom)
    old_zone.boundaries.remove(atom)
    old_zone.atoms.remove(atom)
    atom.zone = zone

    old_zone.point_count = old_zone.point_count - atom.point_count
    zone.point_count = zone.point_count + atom.point_count

    old_zone.polygon = old_zone.polygon.difference(atom.polygon)
    zone.polygon = zone.polygon.union(atom.polygon)

    return (atom,zone,old_zone)

def zone_polygon(zone):
    return cascaded_union([atom.polygon for atom in zone.atoms])

# think about crossing railroad tracks
# we *don't* want railroad tracks to cross

def wiggle_test(num_iter,z):
    v = read_shapes('data/viaducts/Viaducts.shp')
    v_w = [(m,1) for m in v]
    if z == None:
        z = read_zones('data/zones/Patrol Zone.shp')
    a=create_atoms(z,100)
    p = read_shapes('data/points2014/points2014.shp')
    hotspots = read_shapes('data/hotspots/hotspots.shp')
    count(a,p)

    change_atoms = []
    last_zones = z
    starttime = datetime.datetime.now()
    for i in range(num_iter):
        result = wiggle_it(last_zones,p,v_w,hotspots)
        last_zones = result[0]
        if result[1] == None:
            print "FINISHED {} ITERATIONS".format(i + 1)
            print "Score: {}".format(result[3])
            break
        change_atoms.append(result[1])
        print "FINISHED {} ITERATIONS".format(i + 1)
        print "Score: {}".format(result[3])
        print ""

        if i % 10 == 0:
            cur = "{}".format(i + 1)
            save_polygons('data/wiggle10zones/' + cur + '/' + cur + ' iterations.shp',[m.polygon for m in last_zones])
    endtime = datetime.datetime.now()
    td = endtime-starttime
    print td.total_seconds()

    save_polygons('data/wiggle10zones/final/final.shp',[m.polygon for m in last_zones])

    return (last_zones,change_atoms)

# import csv files as well?
# figure out how to deal with different projections

def test_atoms(zone_file, point_file, granularity):
    zones = read_zones(zone_file)
    points = read_shapes(point_file)
    atoms = create_atoms(zones, granularity)

    print len(atoms)

    count(atoms,points)

    print "Workload Scores"
    for zone in zones:
        workload_score_for_zone(zone)
        print "Zone {}: {}".format(zone.number, zone.workload_score)

    print ""
    print "Response Time Scores"
    total_rt = 0
    for zone in zones:
        response_time_score_for_zone(zone)
        print "Zone {}: {}".format(zone.number, zone.response_time_score)
        total_rt += zone.response_time_score
    print total_rt

    return (zones, points, atoms)

def score_map(zone_file):
    zones = read_zones(zone_file)

    atoms = create_atoms(zones,100)

    points = read_shapes('data/points2014/points2014.shp')
    count(atoms,points)

    viaducts = read_shapes('data/viaducts/Viaducts.shp')
    viaducts_with_weights = [(m,1) for m in viaducts]
    hotspots = read_shapes('data/hotspots/hotspots.shp')

    return evaluate_map(zones, points, viaducts_with_weights, hotspots)


# centroid from cascaded union of atoms (for wiggling)